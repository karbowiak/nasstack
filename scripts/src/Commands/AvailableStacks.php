<?php

namespace NASStack\Commands;

use New3den\Console\ConsoleCommand;
use Symfony\Component\Console\Question\ChoiceQuestion;

class AvailableStacks extends ConsoleCommand
{
    protected string $signature = "stacks:available";
    protected string $description = "List all the available stacks";

    public function handle(): void
    {
        $stacksAvailable = array_map(function ($stack) {
            return str_replace('.yml', '', basename($stack));
        }, glob(dirname(__DIR__, 3) . "/stacks/available/*.yml"));
        $stacksEnabled = array_map(function ($stack) {
            return str_replace('.yml', '', basename($stack));
        }, glob(dirname(__DIR__, 3) . "/stacks/enabled/*.yml"));

        // Remove base from enabled
        foreach ($stacksEnabled as $key => $stack) {
            if (strpos($stack, 'base') !== false) {
                unset($stacksEnabled[$key]);
            }
        }

        // Array of stacks that are available but not enabled
        $stacksNotEnabled = array_diff($stacksAvailable, $stacksEnabled);

        $this->tableOneRow($stacksNotEnabled);
    }
}
