<?php

namespace NASStack\Commands;

use New3den\Console\ConsoleCommand;
use Symfony\Component\Console\Question\ChoiceQuestion;

class EnableStack extends ConsoleCommand
{
    protected string $signature = "stacks:enable";
    protected string $description = "List all the available stacks";

    public function handle(): void
    {
        $stacksAvailable = array_map(function ($stack) {
            return str_replace('.yml', '', basename($stack));
        }, glob(dirname(__DIR__, 3) . "/stacks/available/*.yml"));
        $stacksEnabled = array_map(function ($stack) {
            return str_replace('.yml', '', basename($stack));
        }, glob(dirname(__DIR__, 3) . "/stacks/enabled/*.yml"));

        // Remove base from enabled
        foreach ($stacksEnabled as $key => $stack) {
            if (strpos($stack, 'base') !== false) {
                unset($stacksEnabled[$key]);
            }
        }

        // Array of stacks that are available but not enabled
        $stacksNotEnabled = array_diff($stacksAvailable, $stacksEnabled);

        // Symfony console selector
        $dialog = $this->getHelper('question');
        $choises = new ChoiceQuestion('Please select services to enable:', $stacksNotEnabled, 0);
        $choises->setErrorMessage('Selection is not valid');
        $choises->setMaxAttempts(3);
        $choises->setMultiselect(true);

        $selectedStacks = $dialog->ask($this->input, $this->output, $choises);
        $this->out('Stacks to enable: ' . implode(', ', $selectedStacks));

        // Symlink .yml files from stacks/available to stacks/enabled
        foreach ($selectedStacks as $stack) {
            $this->out('Enabling stack: ' . $stack);
            symlink(dirname(__DIR__, 3) . "/stacks/available/{$stack}.yml", dirname(__DIR__, 3) . "/stacks/enabled/{$stack}.yml");
        }
    }
}
