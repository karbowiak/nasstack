SHELL = bash
.DEFAULT_GOAL := help
PWD := $(shell pwd)
FILES := $(shell for file in stacks/enabled/*.yml; do command+=" -f $$file"; done; echo $$command)

help:
	@echo "-- Commands"
	@echo " enableService -- \t\t Enable a service, omit the .yml"
	@echo " listServices -- \t\t List services available and not enabled"
	@echo " htpasswd -- \t\t Generate a htpasswd secret"
	@echo " autheliaSecrets -- \t\t Generate Authelia JWT and Session secrets"
	@echo ""
	@echo "-- Docker"
	@echo " generateCompose -- \t\t Generate docker-compose.yml"
	@echo " up -- \t\t Brings up the containers with logout put to the terminal - only useful for development"
	@echo " stop -- \t\t Stop the containers"
	@echo " start -- \t\t Start the containers"
	@echo " restart -- \t\t Restart the containers"
	@echo " build -- \t\t Pull + Build + Create the containers"

up: | generateCompose
	@docker-compose up --build --always-recreate-deps

restart: | stop start
stop: | generateCompose
	@docker-compose stop

start: | generateCompose
	@docker-compose up -d

build: | generateCompose
	@docker build -t nasstack/php8 build/php8
	@docker-compose pull
	@docker-compose build --no-cache --pull
	@docker-compose create

setup: | autheliaSecrets htpasswd
	@read -p "Input Cloudflare Email: " cloudflareEmail \
	&& /bin/sh -c "echo $${cloudflareEmail} > $(PWD)/secrets/cloudflareEmail"
	@read -p "Input Cloudflare ApiKey: " cloudflareApiKey \
	&& /bin/sh -c "echo $${cloudflareApiKey} > $(PWD)/secrets/cloudflareApiKey"
	@read -p "Input Cloudflare ApiToken: " cloudflareApiToken \
	&& /bin/sh -c "echo $${cloudflareApiToken} > $(PWD)/secrets/cloudflareApiToken"
	@read -p "Input Email Address: " eMail \
	&& /bin/sh -c "echo $${eMail} > $(PWD)/secrets/eMail"
	@read -p "Input Traefik Pilot Token: " traefikPilotToken \
	&& /bin/sh -c "echo $${traefikPilotToken} > $(PWD)/secrets/traefikPilotToken"

autheliaSecrets:
	@cat /dev/urandom | LC_ALL=C tr -dc A-Za-z0-9 | head -c128 > secrets/autheliaJwtSecret
	@cat /dev/urandom | LC_ALL=C tr -dc A-Za-z0-9 | head -c128 > secrets/autheliaSessionSecret
	@echo "Secrets generated"

htpasswd:
	@read -p "Please input username: " username \
	&& /bin/sh -c "htpasswd -c $(PWD)/secrets/htpasswd $${username}"

generateCompose:
	@docker-compose --project-directory $(PWD) $(FILES) config > $(PWD)/docker-compose.yml
	@chmod 600 $(PWD)/config/traefik/acme.json
